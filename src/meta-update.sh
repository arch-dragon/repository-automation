#!/bin/bash

arch=$1

packageDownloadUrl="https://gitlab.com/arch-dragon/packages/meta-packages/-/jobs/artifacts/master/download?job=build"
git clone https://gitlab.com/arch-dragon/packages/meta-packages.git

cd meta-packages

chmod +x ./src/checkIfNeedToBeUpdated.sh

for folder in packages/*; do

    if [ -d "${folder}" ]; then
        packageName=${folder:9}

        ./src/checkIfNeedToBeUpdated.sh $packageName $arch
        
        if [ $? -eq 0 ]; then
            echo "$packageName is up to date"
            continue
        else
            echo "A new version of $packageName will be downloaded"
            wget "${packageDownloadUrl}-${packageName}-${arch}" -O meta-packages.zip
            unzip meta-packages.zip
            mv meta-packages/* ../packages/
            rm meta-packages.zip
            
        fi
    fi

done

cd ..