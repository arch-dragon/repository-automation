#!/bin/bash

arch=$1
option=$2

packageDownloadUrl="https://gitlab.com/arch-dragon/packages/${option}-packages/-/jobs/artifacts/master/download?job=build"

mkdir aur-packages
cd aur-packages

wget "https://gitlab.com/arch-dragon/packages/${option}-packages/-/raw/master/packagelist_$arch" -O "packagelist_$arch"
wget "https://gitlab.com/arch-dragon/packages/${option}-packages/-/jobs/artifacts/master/download?job=setup-$arch" -O pipeline.zip
unzip -o pipeline.zip

while read package; do

    if [[ ! ${package:0:1} == "#" ]]; then
        IFS=';' read -ra packageInfo <<< "${package}"
        packageName=${packageInfo[0]}

        cat "updatelist-$arch.txt" | grep -qi "$packageName "

        if [ $? -eq 0 ]; then
            echo "A new version of $packageName will be downloaded"
            wget "${packageDownloadUrl}-${packageName}-${arch}" -O packages.zip
            unzip packages.zip
            mv packages/* ../packages-aur/
            rm packages.zip
        else
            echo "$packageName is up to date"
            continue
        fi
    fi

done < packagelist_$arch

# mv packages/* ../packages/

cd ..