#!/bin/bash

arch=$1

if [[ "$arch" == "aarch64" ]]; then
    echo "aarch64 not yes supported fro Komikku"
    exit 0
fi

packageDowloadUrl="https://gitlab.com/Snogard/komikku-pkgbuild/-/jobs/artifacts/personal/download?job=build-job"

git clone https://gitlab.com/Snogard/komikku-pkgbuild.git
cd komikku-pkgbuild


if [ ! -f "repolist.txt" ]; then
    curl https://repo.thedragonsden.ovh/archlinux/archdragon/$arch/ > repolist.txt
fi

pkgBuild=PKGBUILD

pkgname=komikku-snogard
pkgver=$(cat $pkgBuild | grep "pkgver=" | sed "s/'//g")
pkgrel=$(cat $pkgBuild | grep "pkgrel=" | sed "s/'//g")

fullname=$pkgname-${pkgver:7}-${pkgrel:7}

echo "$fullname"


if cat repolist.txt | grep -q "$fullname" ; then
    # exit 0 # is up to date
    echo "Komikku is up to date"
else
    # exit 1 # is not up to date
    echo "Komikku will be updated"
    wget $packageDowloadUrl -O packages.zip
    unzip packages.zip -d ../
    rm packages.zip
fi