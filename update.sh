#!/bin/bash

repoName=$1
repoPath=$2
repoArch=$3


if [ -z "$repoName" ]; then
    echo "ERROR: Repository name was not specified"
    echo "usage: ./update.sh repoName [repoPath] [repoArch(x86_64)]"
    exit 1
fi


if [ -z "$repoPath" ]; then
    repoPath="./"
fi

if [ -z "$repoArch" ]; then
    repoArch=x86_64
fi

if [ ! -d "$repoPath" ]; then
    echo "ERROR: $repoPath does not exists"
    exit 2
fi

scriptPath=$(dirname $(realpath $0))

cd $repoPath

rm -rf temp-$repoArch
mkdir temp-$repoArch
cd temp-$repoArch

mkdir packages
mkdir packages-aur

# -----------------------------------------------
echo "Checking AUR updates:"
$scriptPath/src/aur-update.sh $repoArch aur
$scriptPath/src/aur-update.sh $repoArch aur-git
echo "Checking meta updates:"
$scriptPath/src/meta-update.sh $repoArch
echo "Checking komikku updates"
$scriptPath/src/komikku-update.sh $repoArch
# -----------------------------------------------


import_packages(){
    local option=$1

    ls -1 packages${option} > toadd${option}.txt
    mv packages${option}/* "../$repoName${option}/$repoArch"
    cd "../$repoName${option}/$repoArch"

    echo "This packages will be added:"
    cat ../temp-$repoArch/toadd${option}.txt

    while read packageFile; do 
        repo-add -R "${repoName}${option}.db.tar.gz" $packageFile
    done < ../../temp-$repoArch/toadd${option}.txt

    cd ../../temp-$repoArch
}

import_packages
import_packages "-aur"

rm -rf temp-$repoArch

exit 0